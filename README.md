# GitLab Demo

## CLI Merge Requests

To create a merge request, you have to first make a new branch. In the example, I create a branch called `readme_update` that describes what is going on:

```bash
git checkout -b readme_update
```

Then, make any changes necessary, before committing them:

```bash
git commit -m "Update README with CLI merge request tutorial"
```

To push the branch and create a merge request, you add `-o merge_request.create`, and if needing to target a branch other than master, define the target branch with `-o merge_request.target=branch`. I specify the master branch for demonstration purposes:

```bash
git push -u origin readme_update -o merge_request.create -o merge_request.target=master
```

Finally, the merge can be reviewed online, with further commits into that branch automatically appearing in the merge request
